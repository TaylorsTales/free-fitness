import fitness  from '../methods/fitness';
import api_url from '../constants/constants'

// eslint-disable-next-line no-undef
module.exports = {
    constants: {
        api_url,
      // ...
    },
  
    enums: {
      // ...
    },
  
    core: {
      fitness
    },
  
    coreWithDependencies: () => {
      // method: method(config)
    },
  };
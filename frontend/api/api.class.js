import {core, constants} from './config/index'
import baseApiClass from './base-api.class';
const defaultOptions = {};

class fitnessAPI extends baseApiClass.BaseApi {
  constructor(initialOptions) {
    super();
    this.constants = constants;
    this.options = { ...defaultOptions, ...initialOptions };
    this.fitness = core.fitness
    
  }
}


// eslint-disable-next-line no-undef
module.exports = fitnessAPI;

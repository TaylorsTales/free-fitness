import { instance } from "../utilities/axios";
// import {api_url} from "../config/constants"

// const getExercises = (api) => (data) => {
//   return instance.post(`${api.constants.getDomainEnv}/getExercises`, data);
// };

function getWorkout (data) {
  return instance.post(`http://localhost:3000/api/exercise`, data);
}

function getMacros (data) {
  return instance.post(`http://localhost:3000/api/macros`, data);
}

function createRecipes(data){
  return instance.post(`http://localhost:3000/api/recipes`, data);
}

function getRecipes(data){
  return instance.post(`http://localhost:3000/api/recipes`, data);
}

function createExercise(data){
  return instance.post(`http://localhost:3000/api/createExercise`, data);
}

function getMeals(data){
  return instance.post(`http://localhost:3000/api/meals`, data);
}

function getGymCoordinates(data){
  return instance.post(`http://localhost:3000/api/gyms`, data);
}

function getSpecificExercise(data){
  return instance.post(`http://localhost:3000/api/specificExercise`, data);
}

// eslint-disable-next-line no-undef
module.exports = { getWorkout, getMacros, createRecipes, getMeals, getGymCoordinates, createExercise, getRecipes, getSpecificExercise};

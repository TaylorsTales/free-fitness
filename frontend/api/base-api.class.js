const blackListedMethods = [
  "options",
  "constants",
  "logger",
  "_sayHello",
  "getExercises",
];

class BaseApi {
  constructor(){

  }
  toIPC() {
    console.log("Exported interface for IPC communications:");
    
    for (const [key] of Object.entries(this)) {
      (blackListedMethods.includes(key)) || (console.log(`${key}: -> dispatch(${key})`));
    }
  }
}

// eslint-disable-next-line no-undef
module.exports = { BaseApi };
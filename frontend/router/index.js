import Vue from 'vue'
import Router from 'vue-router'

import {routes} from "./routes"
// import { checkAccessMiddleware} from "@/router/Middleware";

Vue.use(Router);

const router = new Router({
  routes,
});



export default router;
export const routes = [
  {
    path: "/",
    name: "Home",
    component: () => import("../components/LandingPage"),
  },
  {
    path: "/fitness",
    name: "Fitness",
    component: () => import("/components/Fitness"),
  },
  {
    path: "/macros",
    name: "Macros",
    component: () => import("/components/Macros"),
  },

  {
    path: "/nutrition",
    name: "Nutrition",
    component: () => import("/components/Nutrition"),
  },

  { path: "/tdee", name: "TDEE", component: () => import("/components/TDEE") },

  {
    path: "/gyms",
    name: "Gym",
    component: () => import("/components/GymFinder"),
  },
  {
    path: "/createExercises",
    name: "CreateExercises",
    component: () => import("/components/CreateExercises"),
  },
  {
    path: "/createRecipes",
    name: "CreateRecipes",
    component: () => import("/components/CreateRecipes"),
  },
  {
    path: "/findRecipes",
    name: "FindRecipes",
    component: () => import("/components/FindRecipes"),
  },
  {
    path: "/findExercises",
    name: "FindExercises",
    component: () => import("/components/FindExercises"),
  }
];

import fitnessAPI from "../../api/api.class";
import "regenerator-runtime/runtime";
const fitnessApi = new fitnessAPI();


const state = {
    exercises: null,
    macros: null,
    recipes:null,
    meals:null,
    gym: null
};

const getters = {
    listExercises(){
        return state => state.exercises;
    },
};

const mutations = {
    SET_EXERCISES(state, payload){
        state.exercises = payload;
    },
    SET_MACROS(state, payload){
        state.macros = payload
    },
    SET_RECIPE(state, payload){
        state.recipes = payload
    },
    SET_MEALS(state, payload){
        state.recipes = payload
    },
    SET_GYM(state, payload){
        state.recipes = payload;
    }
};

const actions = {
    async getExercises({ commit }, data){
        const response = await fitnessApi.fitness.getWorkout(data)
        if(response){
            const payload = response.data;
            commit("SET_EXERCISES", payload);
            return payload;
        }
    },
    async getMacros({ commit }, data){
        const response = await fitnessApi.fitness.getMacros(data);
        if(response){
            const payload = response.data;
            commit("SET_MACROS", payload);
            return payload;
        }
    },
    async createRecipes({ commit }, data){
        const response = await fitnessApi.fitness.createRecipes(data);
        if(response){
            const payload = response.data;
            commit("SET_RECIPE", payload);
            return payload;
        }
    },
    async getMeals({ commit }, data){
        const response = await fitnessApi.fitness.getMeals(data);
        if(response){
            const payload = response.data;
            commit("SET_MEALS", payload);
            return payload;
        }
    },
    async getGymCoordinates({ commit }, data){
        const response = await fitnessApi.fitness.getGymCoordinates(data);
        if(response){
            const payload = response.data;
            commit("SET_GYM", payload);
            return payload;
        }
    },
    async createExercise({ commit }, data){
        const response = await fitnessApi.fitness.createExercise(data);
        if(response){
            const payload = response.data;
            commit("SET_EXERCISES", payload);
            return payload;
        }
    },
    async getRecipes({ commit }, data){
        const response = await fitnessApi.fitness.getRecipes(data);
        if(response){
            const payload = response.data;
            commit("SET_RECIPE", payload);
            return payload;
        }
    },

    async getSpecificExercises({ commit }, data){
        const response = await fitnessApi.fitness.getSpecificExercises(data);
        if(response){
            const payload = response.data;
            commit("SET_EXERCISES", payload);
            return payload;
        }
    },

};

export default {
    state,
    getters,
    actions,
    mutations
  };
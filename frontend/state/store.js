import Vuex from 'vuex';
import Vue from 'vue';
import fitness from './modules/fitness'

// Load Vuex
Vue.use(Vuex);

// Create store
export default new Vuex.Store({
  modules: {
    fitness
  }
});
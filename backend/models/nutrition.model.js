const mongoose = require("mongoose");

const nutritionSchema = new mongoose.Schema({
  cookingTime: Number,
  ingrediants: Array,
  macros: Object,
  mealTime: String,
  name: String,
  dietType: Array,
  allergies: Array,
  calories: Number
});

module.exports = mongoose.model("Nutrition", nutritionSchema);

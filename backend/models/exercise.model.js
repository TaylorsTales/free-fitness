const mongoose = require("mongoose");

const exerciseSchema = new mongoose.Schema({
  ability: String, 
  sets: Number,
  reps: Number,
  type: String,
  name: String,
  body_part: String,
});

module.exports = mongoose.model("Exercises", exerciseSchema);

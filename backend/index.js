const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const connectionString = "mongodb+srv://Chris:Traversetown1@cluster0.myezg.mongodb.net/free-fitness?retryWrites=true&w=majority"

require("dotenv").config();

const app = express();
const port = process.env.PORT || 3000;

app.use(cors());
app.use(express.json());

mongoose.connect(connectionString, {
  useNewUrlParser: true,
  useCreateIndex: true
});

const connection = mongoose.connection;

connection.once("open", () => {
  console.log("MongoDB database connection established successfully");
});

const indexRouter = require("./routes/index");

app.use("/api", indexRouter);

app.listen(port, () => {
  console.log(`Server is running on port: ${port}`);
});

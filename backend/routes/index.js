const express = require("express");
const router = express.Router();
const exercises = require("./exercises");
const nutrition = require("./nutrition");
const macros = require("./macros");
const gym = require("./gym");
const recipe = require("./recipes");
let totalCalories = 0;

//get exercises
router.get("/exercise", function (req, res) {
  Exercises.find(function (err, exercise) {
    res.json(exercise);
  });
});

router.post("/exercise", async function (req, res) {
  const result = await exercises.getExercises(req.body)
  res.send(result)
});

router.post("/createExercise", function (req, res) {
  exercises.createExercise(req.body, res);
});

router.post("/specificExercises", async function (req, res) {
  const result = await exercises.getSpecificExercises(req.body)
  res.send(result)
});


router.post("/meals", function (req, res) {
  nutrition.getMealPlan(req.body, res);
});

router.post("/recipes", async function (req, res) {
  const recipes = req.body.bool == "getRecipes"
    ? await recipe.getRecipes(req.body)
    : recipe.createRecipe(req.body, res);
    res.send(recipes)
});

router.post("/recipes", async (req, res) => {
  const recipes = req.body.bool === "getRecipes"
    ? await recipe.getRecipes(req.body)
    : recipe.createRecipe(req.body, res);
  res.send(recipes)
});

router.post("/macros", function (req, res) {
  macros.getMacros(req.body, res);
});

router.post("/gyms", function (req, res) {
    gym.getGymCoordinates(req.body, res);
});

router.get("/", function (req, res) {
  res.send({ hello: "hello" });
});

module.exports = router;

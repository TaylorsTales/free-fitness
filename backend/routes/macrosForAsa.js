const express = require("express");
const router = express.Router();

// Initially I thought I would create a the functionality of the bmr calculater all in the route. But then I thought it would be a better idea that we have the constents defined in the route and passed to the function.
// v1
function calculateBasalMetabolicRate(height, weight, age, sexValue) {
  return weight + height - age + sexValue;
}

// This is v1 of the route. Which looks good at the moment. However, I just thought that the function calculateBasalMetabolicRate is only used once therefore it would be sensible to use arrow functions instead.
router.post("/macros", (req, res) => {
    
  const weight =
    req.body["unit"] == "metric"
      ? parseFloat(req.body["weight"]) * 10
      : (10 * parseFloat(req.body["weight"])) / 0.15747;
  const height =
    req.body["unit"] == "metric"
      ? 6.25 * parseFloat(req.body["height"])
      : (6.25 * parseFloat(req.body["height"])) / 0.032808;
  const age = 5 * parseFloat(req.body["age"]);
  const sexValue = req.body["sex"] == "male" ? 5 : -161;
  const bmr = calculateBasalMetabolicRate(height, weight, age, sexValue);
});

//v2 No longer need an external function.
router.post("/macros", (req, res) => {
  const weight =
    req.body["unit"] == "metric"
      ? parseFloat(req.body["weight"]) * 10
      : (10 * parseFloat(req.body["weight"])) / 0.15747;
  const height =
    req.body["unit"] == "metric"
      ? 6.25 * parseFloat(req.body["height"])
      : (6.25 * parseFloat(req.body["height"])) / 0.032808;
  const age = 5 * parseFloat(req.body["age"]);
  const sexValue = req.body["sex"] == "male" ? 5 : -161;
  const basalMetabolicRate = () => weight + height - age + sexValue;

  // The next step is to calculate the Total Daily Energy Expenditure
  const userActivityLevel = req.body.activity;
  // My thoughts here are that I am not sure how to make this statement for efficient than it already is.

  // {'sedentary': 1.2}
  const userActivityLevelMultiplier =
    userActivityLevel == "Sedentary"
      ? 1.2
      : userActivityLevel === "Slightly active"
      ? 1.375
      : userActivityLevel === "Lightly Active"
      ? 1.425
      : userActivityLevel === "Moderately Active"
      ? 1.55
      : userActivityLevel === "Very Active"
      ? 1.75
      : userActivityLevel === "Extremely Active"
      ? 1.9
      : 1;
  const totalDailyEnergyExpenditure =
    basalMetabolicRate * userActivityLevelMultiplier;

  // const userActivityLevel = {
  //   "Sedentary": 1.2
  //   "Slightly active": 1.375
  //   "Slightly active": 1.425
  // };



  // Now it is time to calculate the divide of the TDEE into proteins, carbohydrates, and fat for the user.
  //Below was the initial idea of having a goal multiplier but seeing as it would only being used once I re-thought its use.
  //   const goalMultiplier =
  //     req.body["goal"] == "Fat Loss" || "Muscle Gain" ? totalDailyEnergyExpenditure * 0.1 : 0;
  const goalTDEE =
    req.body["goal"] == "Fat Loss"
      ? totalDailyEnergyExpenditure - totalDailyEnergyExpenditure * 0.1
      : req.body["goal"] == "Muscle Gain"
      ? totalDailyEnergyExpenditure + totalDailyEnergyExpenditure * 0.1
      : totalDailyEnergyExpenditure;
  const protein = (goalTDEE * 0.35) / 4;
  const carbohydrates = (goalTDEE * 0.45) / 4;
  const fat = (goalTDEE * 0.2) / 9;
  const macros = {
    tdee: parseInt(goalTDEE, 10),
    protein: parseInt(protein, 10),
    carbohydrates: parseInt(carbohydrates, 10),
    fat: parseInt(fat, 10),
  };
  
  return res.send(macros)
});

const getWeight = ({ unit, weight }) => unit === "Metric" ? parseFloat(weight * 10) : parseFloat(10 * weight) / 0.15747;

// V3 I had to edit the frontend object being passed to the route in order for the right unit to be passed. Turns out it was null when running it. 
// this has been tested via the Ui and works without a hitch. 
router.post("/macros", (req, res) => {
    // Calculate the Basil Metabolic Rate
    const weight = getWeightFromBody(req.body); // === "Metric" ? parseFloat(req.body["weight"] * 10) : parseFloat(10 * req.body["weight"]) / 0.15747;
    const height = req.body.unit === "Metric" ? parseFloat(req.body["height"] * 6.25) : parseFloat(6.25 * req.body["height"]) / 0.032808;
    const age = parseFloat(5 * req.body["age"]);
    const sexValue = req.body["sex"] == "Male" ? 5 : -161;

    // The next step is to calculate the Total Daily Energy Expenditure
    const basalMetabolicRate = weight + height - age + sexValue;
    const userActivityLevel = req.body.activity;
    const userActivityLevelMultiplier =
      userActivityLevel == "Sedentary"
        ? 1.2
        : userActivityLevel == "Slightly active"
        ? 1.375
        : userActivityLevel == "Lightly Active"
        ? 1.425
        : userActivityLevel == "Moderately Active"
        ? 1.55
        : userActivityLevel == "Very Active"
        ? 1.75
        : userActivityLevel == "Extremely Active"
        ? 1.9
        : 1;
    const totalDailyEnergyExpenditure =
      basalMetabolicRate * userActivityLevelMultiplier;
  
    // Calculate the Macronutrients using the user's TDEE  
    const goalTDEE =
      req.body["goal"] == "Fat Loss"
        ? totalDailyEnergyExpenditure - totalDailyEnergyExpenditure * 0.1
        : req.body["goal"] == "Muscle Gain"
        ? totalDailyEnergyExpenditure + totalDailyEnergyExpenditure * 0.1
        : totalDailyEnergyExpenditure;
    const protein = parseInt(((goalTDEE * 0.35) / 4), 10);
    const carbohydrates = (goalTDEE * 0.45) / 4;
    const fat = (goalTDEE * 0.2) / 9;
    const macros = {
      tdee: parseInt(goalTDEE, 10),
      protein,
      carbohydrates: parseInt(carbohydrates, 10),
      fat: parseInt(fat, 10),
    };
        
    return res.send(macros);
  });
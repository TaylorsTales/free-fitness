function getMacros(userInformation, res){
      //Calculate the Basil Metabolic Rate
    const weight = userInformation.unit == "Metric"? parseFloat(userInformation.weight * 10) :  parseFloat(10 * userInformation.weight) / 0.15747;
    const height = userInformation.unit == "Metric"? parseFloat(userInformation.height * 6.25 ) :  parseFloat(6.25 * userInformation.height) / 0.032808;
    const age = parseFloat( 5 *  userInformation.age);
    const sexValue =  userInformation.sex == "Male" ? 5 : -161;
    const basalMetabolicRate = weight + height - age + sexValue;
  
    // The next step is to calculate the Total Daily Energy Expenditure
    const userActivityLevel =  userInformation.activity;
    const userActivityLevelMultiplier =
      userActivityLevel == "Sedentary"
        ? 1.2
        : userActivityLevel == "Slightly active"
        ? 1.375
        : userActivityLevel == "Lightly Active"
        ? 1.425
        : userActivityLevel == "Moderately Active"
        ? 1.55
        : userActivityLevel == "Very Active"
        ? 1.75
        : userActivityLevel == "Extremely Active"
        ? 1.9
        : 1;
    const totalDailyEnergyExpenditure =
      basalMetabolicRate * userActivityLevelMultiplier;
  
    const goalTDEE =
    userInformation.goal == "Fat Loss"
        ? totalDailyEnergyExpenditure - totalDailyEnergyExpenditure * 0.1
        :  userInformation.goal == "Muscle Gain"
        ? totalDailyEnergyExpenditure + totalDailyEnergyExpenditure * 0.1
        : totalDailyEnergyExpenditure;
    const protein = (goalTDEE * 0.35) / 4;
    const carbohydrates = (goalTDEE * 0.45) / 4;
    const fat = (goalTDEE * 0.2) / 9;
    const macros = {
      tdee: parseInt(goalTDEE, 10),
      protein: parseInt(protein, 10),
      carbohydrates: parseInt(carbohydrates, 10),
      fat: parseInt(fat, 10),
    };
    return res.send(macros);
}

module.exports = {getMacros} 
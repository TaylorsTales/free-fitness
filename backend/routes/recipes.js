const Nutrition = require("../models/nutrition.model");

//create recipe
function createRecipe(userInformation, res){
    let nutrition = new Nutrition(userInformation);
    nutrition
      .save()
      .then((nutrition) => {
        res.send(nutrition);
      })
      .catch(function (err) {
        res.status(422).send("Meal plans failed");
      });
} 
  
async function getRecipes(userInformation){
   const recipes = await Nutrition.find({ mealTime: userInformation.timeOfDay }).then( nutrition => nutrition.map(meal => meal.dietType.includes(userInformation.diet)? meal: {}));
   return recipes
}

module.exports = { createRecipe, getRecipes}
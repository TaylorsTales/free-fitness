const axios = require("axios");
// get gym coordinates
function getGymCoordinates(userInformation, res) {
  const options = {
    method: "GET",
    url:
      "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" +
      userInformation.lat +
      "," +
      userInformation.long +
      "&radius=20000&type=gym&key=AIzaSyB6bC_OukDUo3yP4mV6DF9mZ5qFvOmKH-0",
    headers: {},
  };

  axios(options)
    .then(function (response) {
      res.send(JSON.stringify(response.data));
    })
    .catch(function (error) {
      console.log(error);
    });
}
module.exports = { getGymCoordinates };

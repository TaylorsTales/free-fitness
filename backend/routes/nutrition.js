const Nutrition = require("../models/nutrition.model");
let totalCalories = 0;

// Nutrition
function dietRequirements(userInformation, meal) {
  return calorieCalculator(
    userInformation,
    meal.dietType.includes(userInformation.diet)
      ? userInformation.allergies == "none"
        ? meal
        : meal.allergies.includes(userInformation.allergies)
        ? null
        : meal
      : null
  );
}

function calorieCalculator(userInformation, meal) {
  meal != null ? (totalCalories += meal.calories) : 0;
  return totalCalories <= userInformation.calories ? meal : null;
}

function getMealPlan(userInformation, res){
  Nutrition.find().then((mealPlan) =>
  res.json(mealPlan.map((meal) => dietRequirements(userInformation, meal))));
}

module.exports = {getMealPlan} 
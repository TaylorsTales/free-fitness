const Exercises = require("../models/exercise.model");

// This is the old version of the exercise planner creator
function sortExercises(user_information, exercise, cardio, i, j, k, l) {
  return exercise.body_part == "chest" ||
    exercise.body_part == "back" ||
    exercise.body_part == "abs" ||
    exercise.body_part == "biceps"
    ? user_information.days[i] + ", " + user_information.days[k]
    : exercise.body_part == "shoulders" ||
      exercise.body_part == "legs" ||
      exercise.body_part == "abs" ||
      exercise.body_part == "triceps"
    ? user_information.days[j] + ", " + user_information.days[l]
    : cardio;
}
function checkWorkoutDays(user_information, exercise) {
    (exercise["day"] =
      user_information.days.length == 3
        ? user_information.days
        : user_information.days.length == 4
        ? sortExercises(user_information, exercise, null, i, j, k, l)
        : user_information.days.length == 5
        ? sortExercises(
            user_information,
            exercise,
            user_information.days[2],
            (i = 0),
            (j = 1),
            (k = 3),
            (l = 4)
          )
        : user_information.days.length == 6
        ? sortExercises(
            user_information,
            exercise,
            user_information.days[2] + ", " + user_information.days[5],
            (i = 0),
            (j = 1),
            (k = 3),
            (l = 4)
          )
        : sortExercises(
            user_information,
            exercise,
            user_information.days[2] + ", " + user_information.days[5],
            (i = 0),
            (j = 1),
            (k = 3),
            (l = 4)
          ));
  return exercise;
}

async function getExercises(userInformation){
  const exercises = await Exercises.find({ ability: userInformation.ability, type: userInformation.type }).then(exercises => exercises.map(exercise => checkWorkoutDays(userInformation, exercise.toJSON())))
  return exercises
}


// create exercise
function createExercise(userInformation, res) {
  let doesExerciseExist = [];
  Exercises.find(function (err, exercises) {
    doesExerciseExist = exercises.map((exercise) => {
      exercise.name == userInformation.name ? true : false;
    });
  });
  let exercise = new Exercises(userInformation);
  doesExerciseExist.includes(true)
    ? res.send("already exists")
    : exercise
        .save()
        .then((exercise) => {
          res.send(exercise);
        })
        .catch(function (err) {
          res.status(422).send("Meal plans failed");
        });
}

async function getSpecificExercises(userInformation){
  const exercises = await Exercises.find({ ability: userInformation.ability, type: userInformation.workoutLocation, body_part: userInformation.bodyPart})
  return exercises;
}

module.exports = {getExercises, createExercise, getSpecificExercises};
 router.post("/macros", (req, res) => {
   // Calculate BMR
   req.body["unit"] == "metric"
     ? req.body["sex"] == "male"
       ? (req.body.bmr = 10 * parseFloat(req.body["weight"]) + 6.25 * parseFloat(req.body["height"]) - 5 * parseFloat(req.body["age"]) + 5)
       : (req.body.bmr =
           10 * parseFloat(req.body["weight"]) +
           6.25 * parseFloat(req.body["height"]) -
           5 * parseFloat(req.body["age"]) -
           161)
     : req.body["sex"] == "male"
     ? (req.body.bmr =
         (10 * parseFloat(req.body["weight"])) / 0.15747 +
         (6.25 * parseFloat(req.body["height"])) / 0.032808 -
         5 * parseFloat(req.body["age"]) +
         5)
     : (req.body.bmr =
         (10 * parseFloat(req.body["weight"])) / 0.15747 +
         (6.25 * parseFloat(req.body["height"])) / 0.032808 -
         5 * parseFloat(req.body["age"]) -
         161);

   // Calculate TDEE
   req.body.tdee =
     req.body.activity == "Sedentary"
       ? req.body.bmr * 1.2
       : req.body.activity == "Slightly active"
       ? req.body.bmr * 1.375
       : req.body.activity == "Lightly Active"
       ? req.body.bmr * 1.425
       : req.body.activity == "Moderately Active"
       ? req.body.bmr * 1.55
       : req.body.activity == "Very Active"
       ? req.body.bmr * 1.75
       : req.body.activity == "Extremely Active"
       ? req.body.bmr * 1.9
       : null;

   // Calculate Macros
   req.body.macros = {
     tdee: parseInt(req.body.tdee),
     protein: parseInt((req.body.tdee * 0.35) / 4),
     carbohydrates: parseInt((req.body.tdee * 0.45) / 4),
     fat: parseInt((req.body.tdee * 0.2) / 9),
   };

   req.body.macros.tdee =
     req.body["goal"] == "Fat Loss"
       ? req.body.macros.tdee - req.body.macros.tdee * 0.1
       : req.body["goal"] == "Muscle Gain"
       ? req.body.macros.tdee + req.body.macros.tdee * 0.1
       : req.body.macros.tdee;

   req.body.macros = {
     tdee: parseInt(req.body.macros.tdee, 10),
     protein: parseInt((req.body.macros.tdee * 0.35) / 4),
     carbohydrates: parseInt((req.body.macros.tdee * 0.45) / 4),
     fat: parseInt((req.body.macros.tdee * 0.2) / 9),
   };
   return res.send(req.body.macros);
 });


function sortExercises(user_information, exercise, cardio, i, j, k, l) {
    return exercise.body_part == "chest" ||
      exercise.body_part == "back" ||
      exercise.body_part == "abs" ||
      exercise.body_part == "biceps"
      ? user_information.days[i] + ", " + user_information.days[k]
      : exercise.body_part == "shoulders" ||
        exercise.body_part == "legs" ||
        exercise.body_part == "abs" ||
        exercise.body_part == "triceps"
      ? user_information.days[j] + ", " + user_information.days[l]
      : cardio;
  }
  function checkWorkoutDays(user_information, exercise) {
    (i = 0),
      (j = 1),
      (k = 2),
      (l = 3),
      (exercise["day"] =
        user_information.days.length == 3
          ? user_information.days
          : user_information.days.length == 4
          ? sortExercises(user_information, exercise, null, i, j, k, l)
          : user_information.days.length == 5
          ? sortExercises(
              user_information,
              exercise,
              user_information.days[2],
              (i = 0),
              (j = 1),
              (k = 3),
              (l = 4)
            )
          : user_information.days.length == 6
          ? sortExercises(
              user_information,
              exercise,
              user_information.days[2] + ", " + user_information.days[5],
              (i = 0),
              (j = 1),
              (k = 3),
              (l = 4)
            )
          : sortExercises(
              user_information,
              exercise,
              user_information.days[2] + ", " + user_information.days[5],
              (i = 0),
              (j = 1),
              (k = 3),
              (l = 4)
            ));
    return exercise;
  }
  
  router.route("/exercise").post((req, res) => {
    Exercises.find({ ability: req.body.ability, type: req.body.type })
      .then((exercises) =>
        res.json(
          exercises.map((exercise) =>
            checkWorkoutDays(req.body, exercise.toJSON())
          )
        )
      )
      .catch((err) => res.status(400).json("Error: " + err));
  });